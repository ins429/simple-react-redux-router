### Getting started

Install dependencies

```
$ yarn install

# or

$ npm install
```

Then start the server

```
$ yarn start

# or 

$ npm run start
```

And visit http://localhost:8080/


### Running test

```
$ yarn test

# or

$ npm run test
```
