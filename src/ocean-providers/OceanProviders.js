import React from 'react';
import { connect } from 'react-redux';
import ProviderList from 'shared/ProviderList';

const mapStateToProps = ({ provider }) => {
    return {
        title: 'Ocean Providers',
        providers: provider.items.filter(({ type }) => type === 'ocean'),
        isFetching: provider.isFetching,
        errors: provider.errors
    };
};

const OceanProviders = connect(
    mapStateToProps
)(ProviderList);

export default OceanProviders;
