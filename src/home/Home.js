import React from 'react';
import { connect } from 'react-redux';
import ProviderList from 'shared/ProviderList';
import Loader from 'shared/Loader';
import Error from 'shared/Error';
import {
    Link
} from 'react-router-dom';

const Home = ({ isFetching, errors }) => (
    <div className="home">
        {
            ((isFetching, errors = []) => {
                if (isFetching) {
                    return <Loader />;
                } else if (errors.length) {
                    return <Error />;
                } else {
                    return (
                        <div className="home-links">
                            <div className="home-link">
                                <Link className="home-link__link" to="/ocean-providers">Ocean Providers</Link>
                            </div>
                            <div className="home-link">
                                <Link className="home-link__link" to="/air-providers">Air Providers</Link>
                            </div>
                            <div className="home-link">
                                <Link className="home-link__link" to="/customs-brokers">Customs Brokers</Link>
                            </div>
                        </div>
                    );
                }
            })(isFetching, errors)
        }
    </div>
);

const mapStateToProps = ({ provider }) => {
    return {
        providers: provider.items.filter(
            ({ type }) => type === 'customBroker'
        ),
        isFetching: provider.isFetching,
        errors: provider.errors
    };
};

export default connect(
    mapStateToProps
)(Home);
