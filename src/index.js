import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducer';
import { fetchProviders } from './providerActions';
import Home from './home/Home';
import OceanProviders from 'ocean-providers/OceanProviders';
import AirProviders from 'air-providers/AirProviders';
import CustomsBrokers from 'customs-brokers/CustomsBrokers';
import Header from 'shared/Header';
import Footer from 'shared/Footer';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import 'normalize.css';
import 'style.scss';

const store = createStore(reducer, applyMiddleware(
    thunkMiddleware
));

render(
    <Provider store={store}>
        <Router>
            <div className="main">
                <Header />
                <div className="content container">
                    <Route exact path="/" component={Home}/>
                    <Route path="/ocean-providers" component={OceanProviders}/>
                    <Route path="/air-providers" component={AirProviders}/>
                    <Route path="/customs-brokers" component={CustomsBrokers}/>
                </div>
                <Footer />
            </div>
        </Router>
    </Provider>,
    document.getElementById('app')
);

store.dispatch(fetchProviders());
