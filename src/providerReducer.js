import {
    REQUEST_PROVIDERS,
    RECEIVE_PROVIDERS,
    RECEIVE_REQUEST_ERRORS
} from './providerActions';

const initialState = {
    isFetching: false,
    items: [],
    errors: []
};
const provider = (state = initialState, action) => {
    switch (action.type) {
        case REQUEST_PROVIDERS:
            return Object.assign({}, state, {
                isFetching: true,
                errors: []
            });
        case RECEIVE_PROVIDERS:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.providers,
                errors: []
            });
        case RECEIVE_REQUEST_ERRORS:
            return Object.assign({}, state, {
                isFetching: false,
                errors: action.errors
            });
        default:
            return state;
    };
};

export default provider;
