import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
    Link
} from 'react-router-dom';

const getTitle = (pathname) => {
    switch (pathname) {
        case '/ocean-providers':
            return 'Ocean Providers'
        case '/air-providers':
            return 'Air Providers';
        case '/customs-brokers':
            return 'Customs Brokers';
        default:
            return 'Popular Services';
    };
};

const Header = withRouter(props =>
    <header className="header">
        <div className="primary-nav">
            <div className="container primary-nav-container">
                <div className="primary-nav__logo">Demo</div>
                <div className="primary-nav__login-link">Log In</div>
            </div>
        </div>
        <div className="secondary-nav">
            <div className="container">
                <p className="secondary-nav__title">{getTitle(props.location.pathname)}</p>
            </div>
        </div>
    </header>
);

export default Header;
