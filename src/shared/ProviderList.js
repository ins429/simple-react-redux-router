import React from 'react';
import PropTypes from 'prop-types';
import Provider from 'shared/Provider';
import Loader from 'shared/Loader';
import Error from 'shared/Error';

const ProviderList = ({ title, providers, isFetching, errors }) => (
    <div className="provider-list-wrapper">
        <h3 className="provider-list-title">{providers.length} {title}</h3>
        <div className="provider-list">
            {
                ((isFetching, errors = []) => {
                    if (isFetching) {
                        return <Loader />;
                    } else if (errors.length) {
                        return <Error />;
                    } else {
                        return providers.map(
                            (provider, index) => <Provider key={index} {...provider} />
                        );
                    }
                })(isFetching, errors)
            }
        </div>
    </div>
);

ProviderList.propTypes = {
    providers: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired,
    errors: PropTypes.array.isRequired
};

export default ProviderList;
