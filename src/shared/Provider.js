import React from 'react';
import PropTypes from 'prop-types';
import ProviderCompanyLogo from './ProviderCompanyLogo';

const Provider = ({
    companyName,
    images: {
        'Company Logo': companyLogo
    }
}) => (
    <div className="provider">
        <div className="provider-content">
            <ProviderCompanyLogo
                companyName={companyName}
                {...companyLogo}
            />
            <p className="provider__company-name">{companyName}</p>
        </div>
    </div>
);

Provider.propTypes = {
    companyName: PropTypes.string.isRequired,
    images: PropTypes.object.isRequired
};

export default Provider;
