import React from 'react';
import PropTypes from 'prop-types';

const ProviderCompanyLogo = ({ url, companyName }) => (
    <img className="provider__company-logo" src={url} alt={companyName} />
);

ProviderCompanyLogo.propTypes = {
    url: PropTypes.string.isRequired,
    companyName: PropTypes.string.isRequired
};

export default ProviderCompanyLogo;
