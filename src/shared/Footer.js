import React from 'react';
import {
    Link
} from 'react-router-dom';

const Footer = () => (
    <div className="footer">
        <ul className="footer-menu">
            <li><Link className="footer-menu__link" to="/">Home</Link></li>
            <li><Link className="footer-menu__link" to="/ocean-providers">Ocean Providers</Link></li>
            <li><Link className="footer-menu__link" to="/air-providers">Air Providers</Link></li>
            <li><Link className="footer-menu__link footer-menu__link_no-border" to="/customs-brokers">Customs Brokers</Link></li>
        </ul>
        <ul className="footer-sns">
            <li className="footer-sns__link">facebook</li>
            <li className="footer-sns__link">twitter</li>
            <li className="footer-sns__link">instagram</li>
        </ul>
    </div>
);

export default Footer;
