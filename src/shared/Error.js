import React from 'react'

const Error = () => (
    <p className="error">Oops, something went wrong...</p>
)

export default Error
