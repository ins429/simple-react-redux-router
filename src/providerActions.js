import fetch from 'isomorphic-fetch';
export const REQUEST_PROVIDERS = 'REQUEST_PROVIDERS';
export const RECEIVE_PROVIDERS = 'RECEIVE_PROVIDERS';
export const RECEIVE_REQUEST_ERRORS = 'RECEIVE_REQUEST_ERRORS';

export const requestProviders = () => {
    return {
        type: REQUEST_PROVIDERS
    };
};

export const receiveProviders = ({ providers, total }) => {
    return {
        type: RECEIVE_PROVIDERS,
        totalCount: total,
        providers
    };
};

export const receiveRequestErrors = (errors) => {
    return {
        type: RECEIVE_REQUEST_ERRORS,
        errors
    };
};

export const fetchProviders = () => {
    return function(dispatch) {
        dispatch(requestProviders())

        return fetch('https://raw.githubusercontent.com/Shipstr/react-coding-challenge/master/feed/sample.json')
            .then(
                response => {
                    if (response.status >= 200 && response.status < 300) {
                        return response.json();
                    } else {
                        throw new Error('Something went wrong');
                    }
                }
            )
            .then(json => dispatch(receiveProviders(json)))
            .catch((error) => dispatch(receiveRequestErrors([error])));
    };
};
