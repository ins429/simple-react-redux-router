import React from 'react';
import { connect } from 'react-redux';
import ProviderList from 'shared/ProviderList';

const mapStateToProps = ({ provider }) => {
    return {
        title: 'Customs Brokers',
        providers: provider.items.filter(
            ({ type }) => type === 'customBroker'
        ),
        isFetching: provider.isFetching,
        errors: provider.errors
    };
};

const CustomBrokers = connect(
    mapStateToProps
)(ProviderList);

export default CustomBrokers;
