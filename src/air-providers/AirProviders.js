import React from 'react';
import { connect } from 'react-redux';
import ProviderList from 'shared/ProviderList';

const mapStateToProps = ({ provider }) => {
    return {
        title: 'Air Providers',
        providers: provider.items.filter(({ type }) => type === 'air'),
        isFetching: provider.isFetching,
        errors: provider.errors
    };
};

const AirProviders = connect(
    mapStateToProps
)(ProviderList);

export default AirProviders;
