import { combineReducers } from 'redux'
import provider from './providerReducer'

const reducer = combineReducers({
    provider
})

export default reducer
