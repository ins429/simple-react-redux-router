const requiredPropsMatcher = (value) => `\`${value}\` is marked as required`;

export { requiredPropsMatcher };
