import reducer from 'providerReducer';
import {
    REQUEST_PROVIDERS,
    RECEIVE_PROVIDERS,
    RECEIVE_REQUEST_ERRORS
} from 'providerActions';

const initialState = {
    isFetching: false,
    items: [],
    errors: []
};

test('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
});

test('Should handle REQUEST_PROVIDERS', () => {
    expect(
        reducer(undefined, {
            type: REQUEST_PROVIDERS
        })
    ).toEqual({
        isFetching: true,
        items: [],
        errors: []
    });
});

test('Should handle RECEIVE_REQUEST_ERRORS', () => {
    const state = Object.assign({}, initialState, {
        isFetching: true
    });
    const errors = [
        'Something went wrong'
    ];

    expect(
        reducer(state, {
            type: RECEIVE_REQUEST_ERRORS,
            errors: errors
        })
    ).toEqual({
        isFetching: false,
        items: [],
        errors: errors
    });
});

test('Should handle RECEIVE_PROVIDERS', () => {
    const state = Object.assign({}, initialState, {
        isFetching: true
    });
    const providers = [
        {
            companyName: 'test',
            images: {
                'Company Logo': {
                    url: 'https://example.com/fake.jpeg'
                }
            }
        }
    ];

    expect(
        reducer(state, {
            type: RECEIVE_PROVIDERS,
            providers: providers
        })
    ).toEqual({
        isFetching: false,
        items: providers,
        errors: []
    });
});
