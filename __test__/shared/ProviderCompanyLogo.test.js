import React from 'react';
import sinon from 'sinon';
import { shallow, mount, render } from 'enzyme';
import { requiredPropsMatcher } from '../helper';
import ProviderCompanyLogo from 'shared/ProviderCompanyLogo';

const defaultProps = {
    companyName: 'faker',
    url: 'https://example.com/fake.jpeg'
};

test('Should throw console.error when companyName prop is missing', () => {
    const stub = sinon.stub(console, 'error');
    const wrapper = shallow(<ProviderCompanyLogo images={{}} />);

    expect(stub.calledWithMatch(requiredPropsMatcher('url'))).toEqual(true);
    expect(stub.calledWithMatch(requiredPropsMatcher('companyName'))).toEqual(true);
    console.error.restore();
});

test('Should display a provider\' image', () => {
    const wrapper = shallow(<ProviderCompanyLogo {...defaultProps} />);

    expect(wrapper.find('img')).toHaveLength(1);
});
