import React from 'react';
import { shallow } from 'enzyme';
import { requiredPropsMatcher } from '../helper';
import Loader from 'shared/Loader';

test('Should display loading message', () => {
    const wrapper = shallow(<Loader />);

    expect(wrapper.text()).toMatch('Loading...');
});
