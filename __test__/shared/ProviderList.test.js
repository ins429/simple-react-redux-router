import React from 'react';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import { requiredPropsMatcher } from '../helper';
import ProviderList from 'shared/ProviderList';
import Provider from 'shared/Provider';

const defaultProps = {
    providers: [
        {
            companyName: 'test',
            images: {
                'Company Logo': {
                    url: 'https://example.com/fake.jpeg'
                }
            }
        }
    ],
    isFetching: false,
    errors: []
};

test('Should throw console.error when companyName prop is missing', () => {
    const stub = sinon.stub(console, 'error');
    const wrapper = shallow(<ProviderList providers={[]} />);

    expect(stub.calledWithMatch(requiredPropsMatcher('isFetching'))).toEqual(true);
    expect(stub.calledWithMatch(requiredPropsMatcher('errors'))).toEqual(true);
    console.error.restore();
});

test('Should contain Provider components', () => {
    const props = Object.assign({}, defaultProps);
    const wrapper = shallow(<ProviderList {...props} />);

    expect(wrapper.containsMatchingElement(<Provider {...props.providers[0]} />)).toBe(true);
});
