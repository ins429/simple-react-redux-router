import React from 'react';
import sinon from 'sinon';
import { shallow, mount, render } from 'enzyme';
import { requiredPropsMatcher } from '../helper';
import Provider from 'shared/Provider';
import ProviderCompanyLogo from 'shared/ProviderCompanyLogo';

const defaultProps = {
    companyName: 'test',
    images: {
        'Company Logo': {
            url: 'https://example.com/fake.jpeg'
        }
    }
};

test('Should throw console.error when companyName prop is missing', () => {
    const stub = sinon.stub(console, 'error');
    const wrapper = shallow(<Provider images={{}} />);

    expect(stub.calledWithMatch(requiredPropsMatcher('companyName'))).toEqual(true);
    console.error.restore();
});

test('Should displays company name and a ProviderCompanyLogo component', () => {
    const props = Object.assign({}, defaultProps);
    const wrapper = shallow(<Provider {...props} />);

    expect(wrapper.text()).toMatch(props.companyName);
});
