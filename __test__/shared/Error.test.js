import React from 'react';
import { shallow } from 'enzyme';
import { requiredPropsMatcher } from '../helper';
import Error from 'shared/Error';

test('Should display error message', () => {
    const wrapper = shallow(<Error />);

    expect(wrapper.text()).toMatch('Oops, something went wrong...');
});
