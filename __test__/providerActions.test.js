import {
    REQUEST_PROVIDERS,
    RECEIVE_PROVIDERS,
    RECEIVE_REQUEST_ERRORS,
    requestProviders,
    receiveProviders,
    receiveRequestErrors
} from 'providerActions';

test('Should create an action to request providers', () => {
    const expectedAction = {
        type: REQUEST_PROVIDERS
    };

    expect(requestProviders()).toEqual(expectedAction);
});

test('Should create an action to receive providers', () => {
    const json = {
        providers: [
            {
                companyName: 'test',
                images: {
                    'Company Logo': {
                        url: 'https://example.com/fake.jpeg'
                    }
                }
            }
        ],
        total: 1
    };
    const expectedAction = {
        type: RECEIVE_PROVIDERS,
        totalCount: json.total,
        providers: json.providers,
    };

    expect(receiveProviders(json)).toEqual(expectedAction);
});

test('Should create an action to receive request errors', () => {
    const errors = ['Something went wrong'];
    const expectedAction = {
        type: RECEIVE_REQUEST_ERRORS,
        errors
    };

    expect(receiveRequestErrors(errors)).toEqual(expectedAction);
});
